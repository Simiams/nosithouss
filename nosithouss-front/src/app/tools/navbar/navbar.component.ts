import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(private router: Router) { }

  navigateToHome() {
    this.router.navigate(['/']).then(r => console.log("navigateToHome", r))
  }

  navigateToSearch() {
    this.router.navigate(['/search']).then(r => console.log("navigateToSearch", r));
  }

  navigateToAdd() {
    this.router.navigate(['/add']).then(r => console.log("navigateToAdd", r));
  }

  navigateToMap() {
    this.router.navigate(['/map']).then(r => console.log("navigateToMap", r));
  }

  navigateToProfile() {
    this.router.navigate(['/profile']).then(r => console.log("navigateToProfile", r));
  }
}
