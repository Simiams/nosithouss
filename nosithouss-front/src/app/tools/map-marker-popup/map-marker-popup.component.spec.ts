import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapMarkerPopupComponent } from './map-marker-popup.component';

describe('MapMarkerPopupComponent', () => {
  let component: MapMarkerPopupComponent;
  let fixture: ComponentFixture<MapMarkerPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapMarkerPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapMarkerPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
