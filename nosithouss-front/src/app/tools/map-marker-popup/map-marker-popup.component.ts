import { Component, Input } from '@angular/core';
import { IPostGet } from "../../_interfaces/post";
import {Router} from "@angular/router";

@Component({
  selector: 'app-map-marker-popup',
  templateUrl: './map-marker-popup.component.html',
  styleUrls: ['./map-marker-popup.component.css']
})
export class MapMarkerPopupComponent {
  constructor(private router: Router) {  }

  @Input() post!: IPostGet;

  navigateToProfile(username: string) {
    this.router.navigate(['/profile', username]).then(r => console.log("navigateToProfile", r));
  }
}
