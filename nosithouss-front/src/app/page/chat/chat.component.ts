import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {NgClass, NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ChatService} from "src/app/_service/chat.service";
import {IMessageRes} from "src/app/_interfaces/chat/message";
import {Subject, Subscription} from "rxjs";
import {printTimestamp} from "../../_utils/utils";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  standalone: true,
  imports: [
    MatButtonModule,
    MatIconModule,
    NgForOf,
    FormsModule,
    NgClass,
    NgIf,
    NgOptimizedImage
  ],
  styleUrls: ['./chat.component.css']
})

export class ChatComponent implements AfterViewInit, OnInit {
  @ViewChild('scrollContainer') private scrollContainer!: ElementRef;
  private chatService: ChatService;
  user: string = ""
  messages: IMessageRes[] = []
  messageContent: string = "";

  private socket: WebSocket;
  private messageSubject: Subject<string>;
  private messageSubscription: Subscription;

  constructor(chatService: ChatService,
              private route: ActivatedRoute,
              private router: Router) {

    this.user = this.route.snapshot.params['username'];
    this.chatService = chatService

    document.cookie = 'X-Authorization=' + "Bearer " + localStorage.getItem("token");

    this.socket = new WebSocket('ws://localhost:8080/websocket?receiver=' + this.user);

    this.messageSubject = new Subject<string>();

    this.socket.onopen = (event) => {
      console.log('WebSocket connected');
    };

    this.socket.onmessage = (event) => {
      console.log("event")
      console.log(event)
      this.messageSubject.next(event.data);
    };

    this.socket.onerror = (error) => {
      console.error('WebSocket error:', error);
    };

    this.socket.onclose = () => {
      console.log('WebSocket closed');
    };

    this.messageSubject.subscribe(message => {
      console.log('Received message:', message);
    });

    this.messageSubscription = this.messageSubject.subscribe(message => {
      console.log('Received message:', message);
    });

  }

  ngOnInit() {
    this.getMessages()

    this.messageSubscription = this.messageSubject.subscribe(message => {
      this.messages = [...this.messages, {
        ...JSON.parse(message),
        createdAt: printTimestamp(JSON.parse(message).createdAt)
      }]
      console.log('Received message:', this.messages);
    });
  }


  getMessages() {
    this.chatService.getMessages(this.user).subscribe(data => {
        this.messages = data.map(d => ({...d, createdAt: printTimestamp(d.createdAt)}))
        console.log(this.messages)
      },
      err => console.error(err)
    )
  }


  sendMessage(): void {
    if (this.socket.readyState === WebSocket.OPEN) {
      this.socket.send(this.messageContent);
    } else {
      console.error('WebSocket is not open');
    }
  }

  ngAfterViewInit() {
    this.observeChildChanges();
  }

  private scrollToBottom(): void {
    try {
      const element = this.scrollContainer.nativeElement;
      element.scrollTop = element.scrollHeight;
    } catch (err) {
      console.error(err);
    }
  }

  private observeChildChanges(): void {
    const observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.type === 'childList') {
          this.scrollToBottom();
        }
      });
    });
    const config = {childList: true};
    observer.observe(this.scrollContainer.nativeElement, config);
  }

  acceptGuardClaim(message: IMessageRes | null, accept: boolean) {
    message?.id && this.chatService.acceptGuardRequest(accept, message?.id).subscribe(
      response => message.accept = accept,
      error => console.error('Erreur de sendGuardRequest:', error)
    );
  }

  navigateToContact() {
    this.router.navigate(['/contact']).then(r => console.log("navigateToContact", r));
  }
}
