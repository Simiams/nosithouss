import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cgu',
  templateUrl: './cgu.component.html',
  styleUrls: ['./cgu.component.css']
})
export class CguComponent {
  constructor(private location: Location) { }

  navigateBack() {
    this.location.back();
  }
}
