import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import {TokenService} from "../_service/token.service";


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private tokenService: TokenService,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.tokenService.getToken()
    console.log(request.url)
    if(token !== "undefined" && token !== null && !request.url.includes('login')){
      let clone = request.clone({
        headers: request.headers.set('Authorization', 'Bearer '+ token)
      })
      return next.handle(clone).pipe(
        catchError(error => {
          if(error.status === 401){
            this.tokenService.clearTokenExpired()
          }

          return throwError('Session Expired')
        })
      )
    }


    return next.handle(request);
  }
}


export const TokenInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: TokenInterceptor,
  multi: true
}
