
export enum choiceSearch {
  USER = "Personne",
  CATALOG = "Catalogue",
  POST = "Post",
  GUARDINGPOST = "Garde de plante"
}
export enum choiceNewPost {
  CATALOG = "Catalogue",
  POST = "Post",
  GUARDINGPOST = "Garde de plante"
}


export enum EPostType {
  CATALOG,
  GUARDING,
  POST
}

export enum EPostTypeStr {
  CATALOG = "CATALOGUE",
  GUARDING = "GARDE DE PLANTE",
  POST = "POST"
}

export enum MessageType {
  CLAIM_GUARD,
  MESSAGE
}
