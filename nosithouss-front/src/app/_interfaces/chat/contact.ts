export interface IContactRes {
  userName: string,
  lastChat: string,
}
export interface IContactGet {
  userName: string,
  lastChat: number,
}
