export interface ICredential {
  userName: string,
  password: string,
  acceptsCgu?: boolean
}
