package fr.simsim.nosithoussapi.services;

import fr.simsim.nosithoussapi.dtos.responses.ContactRes;
import fr.simsim.nosithoussapi.models.Contact;
import fr.simsim.nosithoussapi.models.User;
import fr.simsim.nosithoussapi.repositories.ContactRepository;
import fr.simsim.nosithoussapi.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactService {

    private final ContactRepository contactRepository;
    private final AuthService authService;

    public ContactService(ContactRepository contactRepository, AuthService authService) {
        this.contactRepository = contactRepository;
        this.authService = authService;
    }

    public void safeSaveContact(Contact contact) {
        Contact newContact = contactRepository.findByUserAndContactUser(contact.getUser(), contact.getContactUser());
        if (newContact == null)
            newContact = contactRepository.findByUserAndContactUser(contact.getContactUser(), contact.getUser());
        contactRepository.save(newContact != null ? newContact.bSetLastChat(contact.getLastChat()) : contact);
    }

    public List<ContactRes> getAllContact() {
        User user = authService.getUser(Utils.getCurrentUserName());
        return contactRepository.findAllByUserOrContactUser(user, user).stream().map(contact -> ContactRes.builder()
                .userName((contact.getContactUser().equals(user) ? contact.getUser() : contact.getContactUser()).getUsername())
                .lastChat(contact.getLastChat())
                .build())
                .toList().reversed();
    }
}
