package fr.simsim.nosithoussapi.services;

import fr.simsim.nosithoussapi.dtos.requests.*;
import fr.simsim.nosithoussapi.models.Contact;
import fr.simsim.nosithoussapi.models.Message;
import fr.simsim.nosithoussapi.models.MessageRequestGuard;
import fr.simsim.nosithoussapi.models.User;
import fr.simsim.nosithoussapi.repositories.MessageGuardRepository;
import fr.simsim.nosithoussapi.repositories.MessageRepository;
import fr.simsim.nosithoussapi.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static fr.simsim.nosithoussapi.utils.Utils.now;

@Service
public class MessageService {

    private final MessageRepository messageRepository;
    private final MessageGuardRepository messageGuardRepository;
    private final ContactService contactService;
    private final PostService postService;
    private final AuthService authService;

    public MessageService(MessageRepository messageRepository, MessageGuardRepository messageGuardRepository, ContactService contactService, PostService postService, AuthService authService) {
        this.messageRepository = messageRepository;
        this.messageGuardRepository = messageGuardRepository;
        this.contactService = contactService;
        this.postService = postService;
        this.authService = authService;
    }

    public Message createMessage(MessageReq messageReq, String userIdentifier) {
        Message message = messageReq.toMessage();
        message.setCreatedAt(now());
        message.setSender(authService.getUser(Utils.getCurrentUserName()));
        message.setReceiver(authService.getUser(userIdentifier));
        contactService.safeSaveContact(Contact.builder()
                .user(message.getSender())
                .lastChat(now())
                .contactUser(message.getReceiver())
                .build());
        return messageRepository.save(message);
    }

    public List<Message> getMessagesByReceiver(String userIdentifier) {
        User currentUser = authService.getUser(Utils.getCurrentUserName());
        return Stream.concat(
                        messageRepository.findAllByReceiverAndSender(currentUser, authService.getUser(userIdentifier)).stream(),
                        messageRepository.findAllByReceiverAndSender(authService.getUser(userIdentifier), currentUser).stream())
                .sorted().toList();
    }

    public void createGuardRequest(MessageGuardReq messageGuardReq, String userIdentifier) {
        messageGuardRepository.save(MessageRequestGuard.builder()
                .post(postService.getPost(messageGuardReq.getPostId()))
                .createdAt(now())
                .content(messageGuardReq.getContent())
                .receiver(authService.getUser(userIdentifier))
                .sender(authService.getUser(Utils.getCurrentUserName()))
                .build());
    }

    public void acceptGuardRequest(MessageGuardAcceptReq messageGuardAcceptReq, Long messageId) {
        Optional<Message> message = messageRepository.findById(messageId);
        if (message.isPresent() && message.get() instanceof MessageRequestGuard messagerequestguard) {
            messageRepository.save(messagerequestguard.bAccept(messageGuardAcceptReq.getAccept()));
            if (Boolean.TRUE.equals(messageGuardAcceptReq.getAccept()))
                postService.addGuardClaimer(ProposalGuardReq.builder().postId(messagerequestguard.getPost().getId()).userName(messagerequestguard.getSender().getUsername()).build());
        }
    }
}
