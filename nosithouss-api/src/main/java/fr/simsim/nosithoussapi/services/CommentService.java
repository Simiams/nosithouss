package fr.simsim.nosithoussapi.services;

import fr.simsim.nosithoussapi.dtos.requests.CommentReq;
import fr.simsim.nosithoussapi.models.Comment;
import fr.simsim.nosithoussapi.models.Post;
import fr.simsim.nosithoussapi.repositories.CommentRepository;
import fr.simsim.nosithoussapi.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.List;

import static fr.simsim.nosithoussapi.utils.Utils.now;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final AuthService authService;
    private final PostService postService;

    public CommentService(CommentRepository commentRepository, AuthService authService, PostService postService) {
        this.commentRepository = commentRepository;
        this.authService = authService;
        this.postService = postService;
    }

    public Comment createComment(CommentReq commentReq) {
        Comment comment = commentReq.toComment();
        comment.setAuthor(authService.getUser(Utils.getCurrentUserName()));
        comment.setPost(postService.getPost(commentReq.getPostId()));
        comment.setCreatedAt(now());
        return commentRepository.save(comment);
    }

    public List<Comment> getCommentsByPostId(Long id) {
        Post post = postService.getPost(id);
        return commentRepository.findAllByPost(post);
    }

}
