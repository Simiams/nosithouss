package fr.simsim.nosithoussapi.services;

import fr.simsim.nosithoussapi.dtos.requests.PostReq;
import fr.simsim.nosithoussapi.dtos.requests.ProposalGuardReq;
import fr.simsim.nosithoussapi.dtos.requests.SeePostsReq;
import fr.simsim.nosithoussapi.dtos.responses.PostRes;
import fr.simsim.nosithoussapi.dtos.responses.PostTitleRes;
import fr.simsim.nosithoussapi.enums.EPostType;
import fr.simsim.nosithoussapi.models.*;
import fr.simsim.nosithoussapi.repositories.PostRepository;
import fr.simsim.nosithoussapi.utils.FileManager;
import fr.simsim.nosithoussapi.utils.PostUtils;
import fr.simsim.nosithoussapi.utils.Utils;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static fr.simsim.nosithoussapi.utils.PostUtils.*;
import static fr.simsim.nosithoussapi.utils.Utils.now;


@Service
public class PostService {

    private final PostRepository postRepository;
    private final AuthService authService;
    private final FileManager fileManager;

    public PostService(PostRepository postRepository, AuthService authService, FileManager fileManager) {
        this.postRepository = postRepository;
        this.authService = authService;
        this.fileManager = fileManager;
    }

    public PostRes createPost(PostReq postReq) {
        User author = authService.getUser(Utils.getCurrentUserName());
        Post post = createPostByPostReq(postReq);
        post.setAuthor(author);
        post.setCreatedAt(now());
        return createPostResponseByPost(postRepository.save(post));
    }

    public List<Post> getPosts(SeePostsReq seePostsReq) {
        Pageable pageable = PageRequest.of(0, seePostsReq.getNumber());
        return postRepository.findByCreatedAtBeforeOrderByCreatedAtDesc(seePostsReq.getCreatedAt(), pageable).getContent();
    }

    public Post updatePost(PostReq postReq, Long id) {
        User author = authService.getUser(Utils.getCurrentUserName());
        Post newPost = createPostByPostReq(postReq);
        newPost.setAuthor(author);
        newPost.setCreatedAt(now());
        newPost.setId(id);
        return postRepository.save(newPost);
    }

    public Post getPost(Long postId) {
        return postRepository.findById(postId).orElseThrow(() -> new RuntimeException("Post not found"));
    }

    public void upload(MultipartFile file, Long postId) throws IOException {
        Image newImage = fileManager.saveImage(file);
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new EntityNotFoundException("Entity not found with id: " + postId));
        if (post instanceof GuardingPost guardingPost) {
            guardingPost.setImages(List.of(newImage.getName()));
        }
        else if (post instanceof CatalogPost catalogPost) {
            catalogPost.setImages(List.of(newImage.getName()));
        }
        postRepository.save(post);
    }

    public List<PostTitleRes> autocomplete(EPostType postType, String prefix) {
        return postRepository.findByTypeAndTitleStartingWith(postType.name().toLowerCase(), prefix)
                .stream().map(p -> new PostTitleRes(p.getTitle(), getFirstImgPostOrNullByPost(p))).toList();
    }

    public void addGuardClaimer(ProposalGuardReq proposalGuardReq) {
        Optional<Post> post = postRepository.findById(proposalGuardReq.getPostId());
        if (post.isPresent() && (Objects.equals(post.get().getAuthor().getUsername(), SecurityContextHolder.getContext().getAuthentication().getName()))) {
            GuardingPost newPost = (GuardingPost) post.get();
            newPost.setGuardClaimer(authService.getUser(proposalGuardReq.getUserName()));
            postRepository.save(newPost);
        }
    }

    public List<PostRes> getOwnPosts() {
        return postRepository.findByAuthor(authService.getUser(Utils.getCurrentUserName())).stream().map(PostUtils::createPostResponseByPost).toList();
    }

    public List<PostRes> getGuardingPosts() {
        return postRepository.findByGuardClaimer(Utils.getCurrentUserName()).stream().map(PostUtils::createPostResponseByPost).toList();
    }

    public List<PostRes> getAllPostByType(EPostType postType) {
        return postRepository.findByType(getPostTypeByEPostType(postType))
                .stream().map(PostUtils::createPostResponseByPost).toList();
    }

    public List<PostRes> getPostsByProfile(String userName) {
        return postRepository.findByAuthor(authService.getUser(userName)).stream().map(PostUtils::createPostResponseByPost).toList();
    }

    public List<PostRes> getGuardinByUsername(String userName) {
        return postRepository.findByGuardClaimer(userName).stream().map(PostUtils::createPostResponseByPost).toList();
    }
}
