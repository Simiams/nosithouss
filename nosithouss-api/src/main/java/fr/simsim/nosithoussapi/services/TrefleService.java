package fr.simsim.nosithoussapi.services;

import fr.simsim.nosithoussapi.client.TrefleClient;
import fr.simsim.nosithoussapi.dtos.requests.TrefleReq;
import fr.simsim.nosithoussapi.enums.EFlag;
import fr.simsim.nosithoussapi.models.CatalogPost;
import fr.simsim.nosithoussapi.models.Flag;
import fr.simsim.nosithoussapi.repositories.FlagRepository;
import fr.simsim.nosithoussapi.repositories.PostRepository;
import fr.simsim.nosithoussapi.utils.FileManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.List;

import static fr.simsim.nosithoussapi.utils.Utils.now;

@Service
public class TrefleService {
    private final TrefleClient trefleClient;
    private final PostRepository postRepository;
    private final FlagRepository flagRepository;
    private final FileManager fileManager;
    private int currentNumber = 0;
    private String lastCommonName;

    @Value("${etl.first-plant-page}")
    private String routeFirstPlantPage;

    public TrefleService(TrefleClient trefleClient, PostRepository postRepository, FlagRepository flagRepository, FileManager fileManager) {
        this.trefleClient = trefleClient;
        this.postRepository = postRepository;
        this.flagRepository = flagRepository;
        this.fileManager = fileManager;
    }

    public void savePlants(int limit) {
        Flag flagPage = flagRepository.findByKey(EFlag.LAST_PAGE.getKey());
        String nextPage = flagPage == null ? routeFirstPlantPage : flagPage.getValue();
        while (!nextPage.isEmpty() && (limit == 0 || limit > currentNumber)) {
            Pair<List<TrefleReq>, String> res = trefleClient.getPlants(nextPage);
            res.getFirst().stream()
                    .map(this::convertToCatalogPost)
                    .map(catalogPost -> (limit == 0 || limit > currentNumber) ? secureSave(catalogPost, res.getSecond()) : null).toList().getLast();
        }
    }

    private CatalogPost convertToCatalogPost(TrefleReq trefleReq) {
        byte[] newImage = trefleClient.getImage(trefleReq.getImageUrl());
        return CatalogPost.builder()
                .title(trefleReq.getCommonName())
                .images(newImage == null ? null : List.of(fileManager.saveImage(newImage).getName()))
                .createdAt(now())
                .build();
    }

    private CatalogPost secureSave(CatalogPost post, String nextPage) {
        if (post != null && post.getTitle() != null && post.getImages() != null) {
            currentNumber += 1;
            lastCommonName = post.getTitle();
            secureSaveFlags(nextPage);
            return postRepository.save(post);
        }
        return null;
    }

    private void secureSaveFlags(String nextPage) {
        flagRepository.save(Flag.builder().key(EFlag.LAST_PAGE.getKey())
                                    .value(nextPage)
                                    .date(now()).build());
        flagRepository.save(Flag.builder().key(EFlag.LAST_COMMON_NAME.getKey())
                                    .value(lastCommonName)
                                    .date(now()).build());
        flagRepository.save(Flag.builder().key(EFlag.LAST_EXTRACTED_NUMBER.getKey())
                                    .value(Integer.toString(currentNumber))
                                    .date(now()).build());
    }
}
