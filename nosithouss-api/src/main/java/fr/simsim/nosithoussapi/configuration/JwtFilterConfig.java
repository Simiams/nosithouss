package fr.simsim.nosithoussapi.configuration;

import fr.simsim.nosithoussapi.services.JwtService;
import fr.simsim.nosithoussapi.services.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Arrays;

@Service
public class JwtFilterConfig extends OncePerRequestFilter {

    private final UserService userService;
    private final JwtService jwtService;

    public JwtFilterConfig(UserService userService, JwtService jwtService) {
        this.userService = userService;
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = null;
        String username = null;
        boolean isTokenExpired = true;

        final String authorization = request.getHeader("Authorization");
        final String cookie = request.getHeader("cookie");

        if (authorization != null && authorization.startsWith("Bearer "))
            token = authorization.substring(7);


        if (cookie != null && cookie.contains("X-Authorization=Bearer"))
            token = Arrays.stream(cookie.split(";")).filter(c -> c.contains("X-Authorization=Bearer")).findFirst().orElse("").split("=")[1].substring(7);


        if (token != null) {
            isTokenExpired = jwtService.isTokenExpired(token);
            username = jwtService.extractUsername(token);
        }

        if (!isTokenExpired && username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userService.loadUserByUsername(username);
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }

        filterChain.doFilter(request, response);
    }
}