package fr.simsim.nosithoussapi.enums;

import lombok.Getter;

@Getter
public enum EMessageType {
    GUARD_CLAIM, MESSAGE;
}
