package fr.simsim.nosithoussapi.utils;

import fr.simsim.nosithoussapi.enums.EMessageType;
import fr.simsim.nosithoussapi.models.Message;
import fr.simsim.nosithoussapi.models.MessageRequestGuard;

public class MessageUtils {

    private MessageUtils() {
    }

    public static EMessageType getEMessageType(Message message) {
        return switch (message) {
            case MessageRequestGuard messageRequestGuard -> EMessageType.GUARD_CLAIM;
            default -> EMessageType.MESSAGE;
        };
    }
}
