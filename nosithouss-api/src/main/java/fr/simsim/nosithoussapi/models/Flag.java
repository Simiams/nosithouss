package fr.simsim.nosithoussapi.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.sql.Timestamp;

@Entity
@Table(name = "flags")
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Flag {
    @Id
    @Column(name = "_key")
    private String key;
    @Column(name = "_value")
    private String value;
    @Column(name = "_date")
    private Timestamp date;
}


