package fr.simsim.nosithoussapi.dtos.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageGuardAcceptReq {
    private Boolean accept;
}
