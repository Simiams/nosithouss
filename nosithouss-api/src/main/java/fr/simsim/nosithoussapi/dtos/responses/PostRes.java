package fr.simsim.nosithoussapi.dtos.responses;

import fr.simsim.nosithoussapi.enums.EPostType;
import fr.simsim.nosithoussapi.models.Post;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

import static fr.simsim.nosithoussapi.utils.PostUtils.getAllImagesByPost;
import static fr.simsim.nosithoussapi.utils.PostUtils.getEPostByPost;

@Getter
@Setter
@AllArgsConstructor
public class PostRes {

    private EPostType type;
    private Long id;
    private String authorUserName;
    private Date createdAt;
    private String content;
    private String title;
    private Post lastVersion;
    private int nbLike;
    private int nbDislike;
    private List<String> images;

    public PostRes(Post post) {
        this.type = getEPostByPost(post);
        this.id = post.getId();
        this.authorUserName = post.getAuthor() != null ? post.getAuthor().getUsername(): "";
        this.createdAt = post.getCreatedAt();
        this.content = post.getContent();
        this.title = post.getTitle();
        this.lastVersion = post.getLastVersion();
        this.nbLike = post.getNbLike();
        this.nbDislike = post.getNbDislike();
        this.images = getAllImagesByPost(post);
    }
}
