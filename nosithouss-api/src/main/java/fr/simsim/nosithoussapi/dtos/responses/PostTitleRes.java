package fr.simsim.nosithoussapi.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class PostTitleRes {

    private String title;
    private String img;
}
