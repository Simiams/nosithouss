package fr.simsim.nosithoussapi.dtos.requests;

import fr.simsim.nosithoussapi.models.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CommentReq {
    private Long postId;
    private String content;

    public Comment toComment() {
        return Comment.builder().content(content).build();
    }
}
