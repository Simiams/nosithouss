package fr.simsim.nosithoussapi.dtos.responses;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContactRes {
    private String userName;
    private Date lastChat;
}
