package fr.simsim.nosithoussapi.dtos.requests;

import fr.simsim.nosithoussapi.enums.EMessageType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageGuardReq extends MessageReq {
    private EMessageType type;
    private Boolean accept;
    private Long postId;

    public MessageGuardReq(String content) {
        super(content);
    }
}
