package fr.simsim.nosithoussapi.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class ProposalGuardReq {
    private Long postId;
    private String userName;
}
