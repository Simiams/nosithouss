package fr.simsim.nosithoussapi.dtos.requests;

import fr.simsim.nosithoussapi.models.Message;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MessageReq {

    private String content;

    public Message toMessage() {
        return Message.builder().content(content).build();
    }
}
