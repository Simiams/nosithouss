package fr.simsim.nosithoussapi.dtos;

import lombok.Builder;
import lombok.Data;
import org.springframework.web.socket.WebSocketSession;

@Data
@Builder
public class InstantContactWs {
    String senderId;
    String receiverId;
    WebSocketSession currentSession;
}
