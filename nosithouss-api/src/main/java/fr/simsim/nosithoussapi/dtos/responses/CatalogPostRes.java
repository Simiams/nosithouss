package fr.simsim.nosithoussapi.dtos.responses;

import fr.simsim.nosithoussapi.models.CatalogPost;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CatalogPostRes extends PostRes {

    private List<String> img;

    public CatalogPostRes(CatalogPost post) {
        super(post);
        this.img = post.getImages();
    }
}
