package fr.simsim.nosithoussapi.dtos.requests;

import fr.simsim.nosithoussapi.models.CatalogPost;
import fr.simsim.nosithoussapi.models.GuardingPost;
import fr.simsim.nosithoussapi.models.Post;
import lombok.Getter;

import java.sql.Timestamp;
import java.util.List;

@Getter
public class PostReq {

    private String type;
    private String content;
    private String title;
    private Post lastVersion;
    private int nbLike;
    private int nbDislike;
    private List<String> img;
    private float coordinateX;
    private float coordinateY;
    private Timestamp guardingAt;
    private Timestamp endGuardingAt;

    public Post toPost() {
        return Post.builder().content(content).title(title).lastVersion(lastVersion).nbLike(nbLike).nbDislike(nbDislike).build();
    }

    public CatalogPost toCatalogPost() {
        return CatalogPost.builder().content(content).title(title).lastVersion(lastVersion).nbLike(nbLike).nbDislike(nbDislike).images(img).build();
    }

    public GuardingPost toGuardingPost() {
        return GuardingPost.builder().content(content).title(title).lastVersion(lastVersion).nbLike(nbLike).nbDislike(nbDislike).images(img).guardingAt(guardingAt).endGuardingAt(endGuardingAt).coordinateX(coordinateX).coordinateY(coordinateY).build();
    }
}
