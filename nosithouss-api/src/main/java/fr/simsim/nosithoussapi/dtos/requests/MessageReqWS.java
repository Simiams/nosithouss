package fr.simsim.nosithoussapi.dtos.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageReqWS {

    private String content;
    private String receiverId;
    private String token;
}
