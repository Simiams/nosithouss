package fr.simsim.nosithoussapi.dtos.responses;

import fr.simsim.nosithoussapi.enums.EMessageType;
import fr.simsim.nosithoussapi.models.Message;
import fr.simsim.nosithoussapi.models.MessageRequestGuard;
import fr.simsim.nosithoussapi.utils.MessageUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class MessageRes {

    private EMessageType type;
    private String senderIdentifier;
    private String content;
    private Date createdAt;
    private Boolean accept;
    private Long postId;
    private Long id;

    public MessageRes(Message message) {
        this.id = message.getId();
        this.senderIdentifier = message.getSender().getUsername();
        this.content = message.getContent();
        this.createdAt = message.getCreatedAt();
        this.type = MessageUtils.getEMessageType(message);
        this.accept = (message instanceof MessageRequestGuard messagerequestguard) ? messagerequestguard.getAccept() : null;
        this.postId = (message instanceof MessageRequestGuard messagerequestguard) ? messagerequestguard.getPost().getId() : null;
    }
}
