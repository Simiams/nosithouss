package fr.simsim.nosithoussapi.dtos.responses;

import fr.simsim.nosithoussapi.models.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class CommentRes {

    private RegisterRes author;
    private String content;

    public CommentRes(Comment comment) {
        this.author = new RegisterRes(comment.getAuthor());
        this.content = comment.getContent();
    }

}
