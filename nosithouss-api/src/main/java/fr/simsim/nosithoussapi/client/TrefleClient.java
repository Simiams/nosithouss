package fr.simsim.nosithoussapi.client;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.simsim.nosithoussapi.dtos.requests.TrefleReq;
import fr.simsim.nosithoussapi.errors.NosithoussException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class TrefleClient {
    private final WebClient webCLient;
    private final ObjectMapper objectMapper;
    private static final String BASE_URL = "https://trefle.io";
    private static final String TOKEN_URL = "&token=q5hC4vxNw4lWucbXgIUcnYirPzAkkdRV6lJDOdILJRo";

    public TrefleClient(WebClient.Builder webClientBuilder, ObjectMapper objectMapper) {
        this.webCLient = webClientBuilder.baseUrl(BASE_URL).build();
        this.objectMapper = objectMapper;
    }

    public Pair<List<TrefleReq>, String> getPlants(String uri) {
        JsonNode trefleRes = webCLient.get()
                .uri(uri + TOKEN_URL)
                .retrieve()
                .bodyToMono(JsonNode.class)
                .block();
        if (trefleRes != null) {
            List<TrefleReq> trefleReqs = new ArrayList<>();
            trefleRes.get("data").forEach(trefle -> trefleReqs.add(objectMapper.convertValue(trefle, TrefleReq.class)));
            return Pair.of(trefleReqs,
                    trefleRes.get("links").has("next") ? trefleRes.get("links").get("next").asText("") : "");
        }
        throw new NosithoussException("Error when try to get plants");
    }
    public byte[] getImage(String url) {
        try {
            return webCLient.get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(byte[].class)
                    .block();
        } catch (Exception e) {
            log.error("[NOSITHOUSS][TrefleClient] Error when try to get Image: {}", e.getMessage());
            return new byte[0];
        }
    }
}
