package fr.simsim.nosithoussapi.repositories;

import fr.simsim.nosithoussapi.models.Image;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface ImageRepository extends JpaRepository<Image, String> {
    Image findByName(String imageUUID);
}
