package fr.simsim.nosithoussapi.repositories;

import fr.simsim.nosithoussapi.models.Message;
import fr.simsim.nosithoussapi.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long>{
    List<Message> findAllBySender(User sender);
    List<Message> findAllByReceiverAndSender(User receiver, User sender);

}
