package fr.simsim.nosithoussapi.repositories;

import fr.simsim.nosithoussapi.models.MessageRequestGuard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageGuardRepository extends JpaRepository<MessageRequestGuard, Long>{

}
