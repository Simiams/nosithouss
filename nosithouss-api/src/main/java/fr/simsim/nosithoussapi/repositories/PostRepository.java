package fr.simsim.nosithoussapi.repositories;


import fr.simsim.nosithoussapi.models.Post;
import fr.simsim.nosithoussapi.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    Page<Post> findByCreatedAtBeforeOrderByCreatedAtDesc(@Param("date") Timestamp date, Pageable pageable);
    List<Post> findByAuthor(@Param("author") User author);

    @Query(value = "SELECT * FROM posts p WHERE LOWER(p.type) = LOWER(:type) AND LOWER(p.title) LIKE CONCAT('%', LOWER(:prefix), '%')", nativeQuery = true)
    List<Post> findByTypeAndTitleStartingWith(@Param("type") String type, @Param("prefix") String prefix);

    @Query(value = "SELECT * FROM posts p WHERE p.author_id = :user AND p.type = 'guarding'", nativeQuery = true)
    List<Post> findByGuardClaimer(@Param("user") String user);

    @Query(value = "SELECT p FROM Post p WHERE TYPE(p) = :type")
    List<Post> findByType(@Param("type") Class<?> type);
}
