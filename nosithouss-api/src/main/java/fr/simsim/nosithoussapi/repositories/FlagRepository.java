package fr.simsim.nosithoussapi.repositories;

import fr.simsim.nosithoussapi.models.Flag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlagRepository extends JpaRepository<Flag, Long> {

    Flag findByKey(String key);
}
