package fr.simsim.nosithoussapi.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.simsim.nosithoussapi.dtos.requests.MessageReq;
import fr.simsim.nosithoussapi.dtos.responses.MessageRes;
import fr.simsim.nosithoussapi.models.Message;
import fr.simsim.nosithoussapi.services.JwtService;
import fr.simsim.nosithoussapi.services.MessageService;
import fr.simsim.nosithoussapi.services.UserService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@RestController
public class WebSocketHandler extends TextWebSocketHandler {
    private final MessageService messageService;
    private final ObjectMapper objectMapper;
    private final UserService userService;
    private final JwtService jwtService;
    private final List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

    public WebSocketHandler(MessageService messageService, ObjectMapper objectMapper, UserService userService, JwtService jwtService) {
        this.messageService = messageService;
        this.objectMapper = objectMapper;
        this.userService = userService;
        this.jwtService = jwtService;
        objectMapper = new ObjectMapper();
    }


    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessions.add(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage msg) throws IOException {
            String token = "";
        List<String> cookie = session.getHandshakeHeaders().get("cookie");

        if (cookie != null)
            token = cookie.stream().map(c -> c.contains("X-Authorization=Bearer")
                    ? Arrays.stream(c.split(";")).filter(c1 -> c1.contains("X-Authorization=Bearer")).findFirst().orElse("").replaceFirst("X-Authorization=Bearer ", "")
                    : "").findFirst().orElse("");


        if (!jwtService.isTokenExpired(token) && jwtService.extractUsername(token) != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userService.loadUserByUsername(jwtService.extractUsername(token));
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }

        String receiver;
        if (session.getUri() != null)
            receiver = session.getUri().getQuery().split("=")[1];
        else {
            receiver = "";
        }

        WebSocketSession receiverSession = sessions.stream().filter(s -> s.getPrincipal().getName().equals(receiver)).findFirst().orElse(null);


        Message newMessage = messageService.createMessage(new MessageReq(msg.getPayload()), receiver);

        if (receiverSession != null) {
            receiverSession.sendMessage(new TextMessage(objectMapper.writeValueAsString(new MessageRes(newMessage))));
        }

        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(new MessageRes(newMessage))));
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessions.remove(session);
    }
}
