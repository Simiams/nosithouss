package fr.simsim.nosithoussapi.controllers;

import fr.simsim.nosithoussapi.errors.NosithoussException;
import fr.simsim.nosithoussapi.services.TrefleService;
import fr.simsim.nosithoussapi.utils.FileManager;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


//@RestController
//@RequestMapping("/websocket")
public class Test {
    private final TrefleService trefleService;
    private final FileManager fileManager;
    public Test(TrefleService trefleService, FileManager fileManager) {
        this.trefleService = trefleService;
        this.fileManager = fileManager;
    }

    @GetMapping(value = "/exception")
    @Operation(summary = "[TEST] Test the exceptions")
    public String exception() {
        throw new NosithoussException("test Exception");
    }

    @GetMapping(value = "/{word}")
    @Operation(summary = "[TEST] Test the api with PathVariable")
    public String auth(@PathVariable String word) {
        return word;
    }

    @GetMapping(value = "/hello")
    @Operation(summary = "[TEST] Test say hello")
    public String helloWord() {
        return "Hello World!";
    }

    @GetMapping(value = "/user")
    @Operation(summary = "[TEST] Test get the current user based on his token")
    public String getUser() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @PostMapping("/upload")
    @Operation(summary = "[TEST] Test upload file")
    public String uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        fileManager.saveImage(file);
        return "ok";
    }

    @GetMapping("/etl")
    @Operation(summary = "[TEST] Test start the etl")
    public String savePlants() {
        trefleService.savePlants(10);
        return "Ok";
    }

    @GetMapping("/info")
    @Operation(summary = "[TEST] Test start the etl")
    public String zerfghj(@RequestParam String t) {
        return "Ok";
    }
}
