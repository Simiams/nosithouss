package fr.simsim.nosithoussapi.controllers;

import fr.simsim.nosithoussapi.dtos.responses.ContactRes;
import fr.simsim.nosithoussapi.services.ContactService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contact")
public class ContactController {

    private final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping(value = "")
    @Operation(summary = "Get all the user you send a message or who have sent you message, base on your token")
    public ResponseEntity<List<ContactRes>> getAllContact() {
        return ResponseEntity.ok(contactService.getAllContact());
    }

    @PostMapping("")
    public void addContact(@RequestBody ContactRes contact) {
    }

    @PutMapping("/{contact_id}")
    public void updateContact(@PathVariable Long contact_id, @RequestBody ContactRes contact) {
    }

    @PatchMapping("/{contact_id}")
    public void patchContact(@PathVariable Long contact_id, @RequestBody ContactRes contact) {
    }

    @DeleteMapping("/{contact_id}")
    public void deleteContact(@PathVariable Long contact_id) {
    }
}

