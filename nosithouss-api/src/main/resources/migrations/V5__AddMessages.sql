-- nolwax => oussouss
INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'nolwax', 'oussouss', 'Salut Oussouss ! 🌿 Tu as des conseils pour prendre soin de mes nouvelles plantes d''intérieur ?', '2024-02-11 22:00:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'oussouss', 'nolwax', 'Salut Nolwax ! Bien sûr, je suis passionné(e) de botanique. 🌱 Pour commencer, quel type de plantes as-tu ? 😊', '2024-02-11 22:05:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'nolwax', 'oussouss', 'J''ai des roses, des orchidées et quelques plantes grasses. Des astuces spécifiques pour chacune d''entre elles ? 🌹🌵', '2024-02-11 22:10:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'oussouss', 'nolwax', 'Absolument ! Pour les roses, il est important de bien les tailler et de les arroser régulièrement. Les orchidées préfèrent la lumière indirecte et une humidité modérée. Quant aux plantes grasses, elles aiment le soleil et n''ont pas besoin de beaucoup d''eau. 🌞💦', '2024-02-11 22:15:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'nolwax', 'oussouss', 'Merci pour les conseils, Oussouss ! 😊 J''essaierai de mettre tout cela en pratique. Si tu as d''autres astuces, je suis preneur(euse) ! 🌿', '2024-02-11 22:20:00');


-- simsim => nolwax
INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'simsim', 'nolwax', 'Salut Nolwax ! J''ai entendu dire que tu es calé(e) en botanique. Tu peux me donner des conseils pour faire pousser des herbes aromatiques à la maison ? 🌿', '2024-02-11 22:30:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'nolwax', 'simsim', 'Salut Simsim ! Bien sûr, je serais ravi(e) de t''aider. Quelles herbes aromatiques souhaites-tu faire pousser ? 🌱', '2024-02-11 22:35:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'simsim', 'nolwax', 'Je pense à la menthe, au basilic et à la coriandre. Des suggestions sur la façon de les cultiver à la maison ? 🌱', '2024-02-11 22:40:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'nolwax', 'simsim', 'Absolument ! La menthe et la coriandre aiment l''humidité, tandis que le basilic a besoin de beaucoup de lumière. Assure-toi de bien arroser et de leur fournir un sol bien drainé. 🌞💧', '2024-02-11 22:45:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'simsim', 'nolwax', 'Merci pour les conseils, Nolwax ! J''ai hâte de commencer mon petit jardin d''herbes aromatiques. 🌿😊', '2024-02-11 22:50:00');


-- oussouss => simsim
INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'oussouss', 'simsim', 'Salut Simsim ! 🌸 J''ai entendu dire que tu es aussi fan de plantes. Tu as des astuces pour prendre soin des plantes d''intérieur en hiver ? ❄️', '2024-02-11 23:00:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'simsim', 'oussouss', 'Salut Oussouss ! Absolument, l''hiver peut être délicat pour certaines plantes. Assure-toi de ne pas les exposer à des températures trop froides, et essaie de maintenir une humidité adéquate. Certaines plantes peuvent bénéficier d''une lumière artificielle supplémentaire. 🌬️🌿', '2024-02-11 23:05:00');

INSERT INTO public.messages (type, sender_id, receiver_id, content, created_at)
VALUES ('message', 'oussouss', 'simsim', 'Merci pour les conseils, Simsim ! 😊 Je vais prendre toutes ces précautions pour mes plantes. Si tu as d''autres conseils hivernaux, je suis preneur(euse) ! 🌨️🌱', '2024-02-11 23:10:00');