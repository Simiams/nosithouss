ALTER TABLE public.contacts DROP CONSTRAINT fkbwdla3daipku5af60p84menq5;
ALTER TABLE public.contacts ADD CONSTRAINT contacts_contact_id_fk_users FOREIGN KEY (contact_id) REFERENCES public.users(user_name);

ALTER TABLE public.contacts DROP CONSTRAINT fkna8bddygr3l3kq1imghgcskt8;
ALTER TABLE public.contacts ADD CONSTRAINT contacts_user_id_fk_users FOREIGN KEY (user_id) REFERENCES public.users(user_name);

ALTER TABLE public.posts DROP CONSTRAINT fk6xvn0811tkyo3nfjk2xvqx6ns;
ALTER TABLE public.posts ADD CONSTRAINT posts_author_id_fk_users FOREIGN KEY (author_id) REFERENCES public.users(user_name);

ALTER TABLE public.posts DROP CONSTRAINT fk268l0ibw80sligk73yx3vdi8c;
ALTER TABLE public.posts ADD CONSTRAINT posts_last_version_id_fk_posts FOREIGN KEY (last_version_id) REFERENCES public.posts(id);

ALTER TABLE public.posts DROP CONSTRAINT fkobac4feo24nork1nuikismtt;
ALTER TABLE public.posts ADD CONSTRAINT posts_guard_claimer_fk_users FOREIGN KEY (guard_claimer) REFERENCES public.users(user_name);

ALTER TABLE public.comments DROP CONSTRAINT fkn2na60ukhs76ibtpt9burkm27;
ALTER TABLE public.comments ADD CONSTRAINT comments_author_id_fk_users FOREIGN KEY (author_id) REFERENCES public.users(user_name);

ALTER TABLE public.comments DROP CONSTRAINT fkh4c7lvsc298whoyd4w9ta25cr;
ALTER TABLE public.comments ADD CONSTRAINT comments_post_id_fk_posts FOREIGN KEY (post_id) REFERENCES public.posts(id);

ALTER TABLE public.messages DROP CONSTRAINT fkt05r0b6n0iis8u7dfna4xdh73;
ALTER TABLE public.messages ADD CONSTRAINT messages_receiver_id_fk_users FOREIGN KEY (receiver_id) REFERENCES public.users(user_name);

ALTER TABLE public.messages DROP CONSTRAINT fk4ui4nnwntodh6wjvck53dbk9m;
ALTER TABLE public.messages ADD CONSTRAINT messages_sender_id_fk_users FOREIGN KEY (sender_id) REFERENCES public.users(user_name);

ALTER TABLE public.messages DROP CONSTRAINT fk86vxjyfsjfddqi277p88mjt3f;
ALTER TABLE public.messages ADD CONSTRAINT messages_post_id_fk_posts FOREIGN KEY (post_id) REFERENCES public.posts(id);




CREATE INDEX idx_users_username ON public.users(user_name);
CREATE INDEX idx_posts_title ON public.posts(title);
CREATE INDEX idx_messages_sender_id ON public.messages(sender_id);
CREATE INDEX idx_messages_receiver_id ON public.messages(receiver_id);