package fr.simsim.nosithoussauth.repositories;

import fr.simsim.nosithoussauth.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUserName(String username);

    List<User> findByUserNameStartingWith(String prefix);
}
