package fr.simsim.nosithoussauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NosithoussAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(NosithoussAuthApplication.class, args);
    }
}
