package fr.simsim.nosithoussauth.controllers;

import fr.simsim.nosithoussauth.dtos.requests.AuthenticationReq;
import fr.simsim.nosithoussauth.dtos.responses.AuthLoginRes;
import fr.simsim.nosithoussauth.models.User;
import fr.simsim.nosithoussauth.services.JwtService;
import fr.simsim.nosithoussauth.services.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/auth")
public class AuthController {
    private AuthenticationManager authenticationManager;
    private UserService userService;
    private JwtService jwtService;


    @PostMapping("/register")
    public void register(@RequestBody User user) {
        log.info("[NOSITHOUS] [auth] Post api/auth/register");
        userService.register(user);
    }


    @PostMapping("/login")
    public AuthLoginRes login(@RequestBody AuthenticationReq authenticationReq) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationReq.userName(), authenticationReq.password()));

        if (authenticate.isAuthenticated()) {
            Map<String, String> jwt = jwtService.generate(authenticationReq.userName());
            return new AuthLoginRes(jwt.get("bearer"), authenticationReq.userName());
        }
        throw new RuntimeException("Authentication failed");
    }
}
