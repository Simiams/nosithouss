package fr.simsim.nosithoussauth.dtos.responses;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AuthLoginRes {
    private String token;
    private String username;
}
