package fr.simsim.nosithoussauth.dtos.responses;

import fr.simsim.nosithoussauth.enums.ERole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class UserRes {
    private String userName;
    private String firstName;
    private String lastName;
    private Set<ERole> roles;
    private String pdp;
}
